/// <reference types="cypress" />

const { FooterPage } = require("../page-objects/footer-page");
const footerPage = new FooterPage();

describe('Validate Link Text to Link URLs', () => {
    const liveCasino = 'live casino';
    const paymentMethods = 'payment methods';
    const sectionOfTableGames = 'selection of table games';
    const noDepositBonuses = 'no deposit bonuses';
    const topQualityCasinoSites = 'top-quality online casino sites';

    beforeEach(() => {
        cy.visit('/real-money/');
    });

    it(`should validate link ${noDepositBonuses}`, () => {
        footerPage.assertLinkTextToLink(noDepositBonuses, '/no-deposit-bonuses');
    });

    it(`should validate link to ${liveCasino}`, () => {
        footerPage.assertLinkTextToLink(liveCasino, '/live-dealer/')
    });

    it(`should validate link to ${paymentMethods}`, () => {
        footerPage.assertLinkTextToLink(paymentMethods, '/deposit-methods/')
    });

    it(`should validate link to ${sectionOfTableGames}`, () => {
        footerPage.assertLinkTextToLink(sectionOfTableGames, '/table-games/')
    });

    it(`should validate link to ${topQualityCasinoSites}`, () => {
        footerPage.assertLinkTextToLink(topQualityCasinoSites, '/reviews/')
    });

});