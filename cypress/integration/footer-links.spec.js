/// <reference types="cypress" />

const { FooterPage } = require("../page-objects/footer-page");
const footerPage = new FooterPage();

describe('Footer links', () => {

    beforeEach(() => {
        cy.visit('/');
    });

    it(`should validate 1st link`, () => {
        footerPage.footerLinkValidation(1, '/contact');
    });

    it(`should validate 2nd link`, () => {
        footerPage.footerLinkValidation(2, '/responsible-gambling');
    });

    it(`should validate 3rd link`, () => {
        footerPage.footerLinkValidation(3, '/gaming-fairness-and-testing-companies');
    });

    it(`should validate 4th link`, () => {
        footerPage.footerLinkValidation(4, '/online-gaming-regulators-and-licensing-bodies');
    });

    it(`should validate 5th link`, () => {
        footerPage.footerLinkValidation(5, '/sitemap.html');
    });
    
    it(`should validate 6th link`, () => {
        footerPage.footerLinkValidation(6, '/policy');
    });

});