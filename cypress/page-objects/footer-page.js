export class FooterPage {
    assertLinkTextToLink(linkName, hrefLink) {
        cy.contains(linkName).first().click({ force: true });
        cy.server()
        cy.route({
          method: 'GET',
          url: `${hrefLink}`
        }).then(respone => {
            expect(respone.status).to.eq(200)
        });
        cy.url().should('contain', hrefLink)
    }

    footerLinkValidation(linkNumber, linkPartial) {
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Application error skipped on purpose')
        
            return false
          });
        cy.get(`.col-md-9 > ul > :nth-child(${linkNumber}) > a`).click( {force: true});
        cy.url().should('contain', linkPartial)
    }
}