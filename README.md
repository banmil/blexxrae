# README #

This repo contains solution for Blexr QA. It uses CypressIO framework for E2E testing based on JavaScript

### Prerequisites ###

- Install node from vendor solution

### Setup environemnt to run tests: ###

- Install CypressIO from vendor solution:

1. npm install cypress --save-dev

- Install some IDE -> recommend Visual Studio Code

### How do I run tests: ###

- clone the project
- run the command: npx cypress run in terminal
- to run in headless mode run the command npx cypress run --headless
- to run a specific test file run the command with --spec flag

